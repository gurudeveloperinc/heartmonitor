<?php
session_start();
error_reporting(0);

if (!isset($_SESSION['healthid'])) {
	header("Location: login.php");
}

require 'database.php';


$healthid = $_SESSION['healthid'];

$bpmQuery = "select * from bpm where healthid = '$healthid' ORDER BY created_at desc";
$bpms = $mysqli->query($bpmQuery);

?>



<link href="//db.onlinewebfonts.com/c/a4e256ed67403c6ad5d43937ed48a77b?family=Core+Sans+N+W01+35+Light" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="style.css" type="text/css">
<script src="js/jquery.js"></script>
<div class="body-content">
	<h3 align="center" class="white">BIO-BANK DASHBOARD</h3>

	<div class="index">

		<div class="menu">
			<h5 class="black"><?php echo "Welcome " . $_SESSION['username'] ?></h5> <br><br>
			<h4 class="black">Health ID: <span style="color:green"><?php echo $_SESSION['healthid']; ?></span> </h4> <br><br>
			<h4 class="black">Email: <?php echo $_SESSION['email']; ?></h4> <br><br>
			<h3 class="black">Phone: <?php echo $_SESSION['phone']; ?></h3> <br><br>
			<a href="index.php" class="btn btn-primary">Dashboard</a> <br> <br>
			<a href="logout.php" class="btn btn-primary">Logout</a>
		</div>
		<div class="content">

			<table class="table">
				<tr>
					<th>BPM</th>
					<th>Date</th>
				</tr>

				<?php

				while($resultRows = mysqli_fetch_assoc($bpms)){
					echo "<tr>";
					echo "<td>". $resultRows['bpm'] . "</td>";
					echo "<td>". $resultRows['created_at'] . "</td>";
					echo "</tr>";
				}
				?>
			</table>
		</div>

	</div>

</div>