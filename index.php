<?php 
session_start();
error_reporting(0);

if (!isset($_SESSION['healthid'])) {
	header("Location: login.php");
}

require 'database.php';


$healthid = $_SESSION['healthid'];

$bpmQuery = "select * from bpm where healthid = '$healthid' ORDER BY created_at desc limit 5";
$bpms = $mysqli->query($bpmQuery);

?>



<link href="//db.onlinewebfonts.com/c/a4e256ed67403c6ad5d43937ed48a77b?family=Core+Sans+N+W01+35+Light" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/morris-0.4.3.min.css" type="text/css">
<script src="js/jquery.js"></script>
<script src="js/raphael-2.1.0.min.js"></script>
<script src="js/morris.js"></script>

<div class="body-content">
    <h3 align="center" class="white">BIO-BANK DASHBOARD</h3>

      <div class="index">

          <div class="menu">
              <h5 class="black"><?php echo "Welcome " . $_SESSION['username'] ?></h5> <br><br>
              <h4 class="black">Health ID: <span style="color:green"><?php echo $_SESSION['healthid']; ?></span> </h4> <br><br>
              <h4 class="black">Email: <?php echo $_SESSION['email']; ?></h4> <br><br>
              <h3 class="black">Phone: <?php echo $_SESSION['phone']; ?></h3> <br><br>
              <a href="history.php" class="btn btn-primary">BPM History</a> <br> <br>
              <a href="logout.php" class="btn btn-primary">Logout</a>
          </div>
          <div class="content">

              <div>
                  <p>Last known BPM: <span id="beat" style="color: green; font-weight: 700"> <?php
                      $row = mysqli_fetch_row($bpms);

                      echo $row[1];
                      echo "</span><br> Recorded at : ";
                      echo $row[3];
                      ?>
                  </p>

                  <div style="min-height: 150px;" align="center">
                      <img src="images/heart.png" id="heart" style="width: 100px;">
                  </div>


                  <style>
                      .transition{
                          -webkit-transition: width 90ms linear;
                          -moz-transition: width 90ms linear;
                          -ms-transition: width 90ms linear;
                          -o-transition: width 90ms linear;
                          transition: width 90ms linear;
                      }

                  </style>
                  <script>
                      $(document).ready(function(){

                          var lastBeat = $('#beat').text();
                          var division = 60000/ lastBeat;

                          console.log('division  ' + division);
                          console.log('last beat ' + lastBeat);
                          var heart = $('#heart');

                          setInterval(function(){
                             heart.css('width','120px');
                             heart.addClass('transition');

                              setTimeout(function(){
                                  heart.css('width','100px');
                                  heart.addClass('transition');
                              }, 120);

                          },division);


                          var chartData;

                          $.ajax({
                              url:"getBPM.php?healthid=<?php echo $_SESSION['healthid']; ?>",
                              method: "get",
                              success: function (response) {


                                  $('#morris-dashboard-chart').html("");
                                  chartData = JSON.parse(response);
                                  $('#beat').text(chartData[chartData.length - 1].visits);
                                  console.log(chartData);
                                  var morris1 = Morris.Area({
                                      element: 'morris-dashboard-chart',
                                      data: chartData,
                                      xkey: 'period',
                                      ykeys: ['visits'],
                                      labels: ['BPM', 'Time'],
                                      pointSize: 2,
                                      hideHover: 'true',
                                      resize: true,
                                      lineColors: ['#1abc9c', '#6ce3cc']
                                  });
                                  morris1.redraw();
                              },
                              error: function (response) {

                                  console.log(response);
                              }
                          });



                          setInterval(function () {

                              var chartData;


                              $.ajax({
                                  url:"getBPM.php?healthid=<?php echo $_SESSION['healthid']; ?>",
                                  method: "get",
                                  success: function (response) {

                                      $('#morris-dashboard-chart').html("");
                                      chartData = JSON.parse(response);
                                      $('#beat').text(chartData[chartData.length - 1].visits);
                                      console.log(chartData);
                                      var morris1 = Morris.Area({
                                          element: 'morris-dashboard-chart',
                                          data: chartData,
                                          xkey: 'period',
                                          ykeys: ['visits'],
                                          labels: ['BPM', 'Time'],
                                          pointSize: 2,
                                          hideHover: 'true',
                                          resize: true,
                                          lineColors: ['#1abc9c', '#6ce3cc']
                                      });
                                      morris1.redraw();
                                  },
                                  error: function (response) {

                                      console.log(response);
                                  }
                              });




                          },10000);




                      });
                  </script>

              </div>
              <table class="table">
                  <tr>
                      <th>BPM</th>
                      <th>Date</th>
                  </tr>

                  <?php

                  while($resultRows = mysqli_fetch_assoc($bpms)){
                      echo "<tr>";
                      echo "<td>". $resultRows['bpm'] . "</td>";
                      echo "<td>". $resultRows['created_at'] . "</td>";
                      echo "</tr>";
                  }
                  ?>
              </table>

              <div class="chart" id="morris-dashboard-chart"></div>
          </div>

      </div>

</div>