<?php
session_start();

if (isset($_SESSION['healthid'])) {
	header("Location: index.php");
}

require 'database.php';


$_SESSION['message']= '';


$mysqli= new mysqli('localhost','root','','health_records');


if($_SERVER['REQUEST_METHOD']== 'POST') {
	// TWO PASSWORDS MATCH
	if($_POST['passcode']== $_POST['confirmpasscode']) {

		$username= $mysqli->real_escape_string($_POST['username']);
		$email = $mysqli->real_escape_string($_POST['email']);
		$phone = $mysqli->real_escape_string($_POST['phone']);
		$passcode= md5($_POST['passcode']);

		$healthid = random_str(9);


		$_SESSION['username']= $username;
		$_SESSION['passcode']= $passcode;
		$_SESSION['healthid'] = $healthid;
		$sql= "INSERT INTO users(username,email,passcode,healthid,phone)"
		      . "VALUES('$username','$email','$passcode','$healthid','$phone')";

		// if query is successful redirect to welcome page
		if ($mysqli->query($sql)=== true ){

			sendSms($phone,"Your health ID is $healthid. Thank you for registering with us.");
			$_SESSION['message']= "registration successful";
			header("location: index.html");
		} else {
			$_SESSION['message'] = "An error occurred at registration. Please try again!";
		}

	} else {
		$_SESSION['message'] = "The two passcodes dont match!";
	}
}


// random string generator
function random_str($length, $keyspace = '0123456789')
{

	$str = '';
	$max = mb_strlen($keyspace, '8bit') - 1;
	for ($i = 0; $i < $length; ++$i) {
		$str .= $keyspace[ rand(0, $max)];
	}
	return $str;
}

function sendSms($phone,$Message){

	/* Variables with the values to be sent. */
	$owneremail="tobennaa@gmail.com";
	$subacct="tessel";
	$subacctpwd="tessel";
	$sendto= $phone; /* destination number */
	$sender="Health ID"; /* sender id */

	$message= $Message;  /* message to be sent */

	/* create the required URL */
	$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
	       . "&subacct=" . UrlEncode($subacct)
	       . "&subacctpwd=" . UrlEncode($subacctpwd)
	       . "&message=" . UrlEncode($message)
	       . "&sender=" . UrlEncode($sender)
	       ."&sendto=" . UrlEncode($sendto)
	       ."&msgtype=0";

	file_get_contents($url);

}




?>



<link href="//db.onlinewebfonts.com/c/a4e256ed67403c6ad5d43937ed48a77b?family=Core+Sans+N+W01+35+Light" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/style.css" type="text/css">
<div class="body-content">
    <div class="module">
        <h1 class="white" align="center">BIO-BANK REGISTRATION</h1>
        <form class="form" action="index.php" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="alert alert-error"><?= $_SESSION['message'] ?></div>
            <input type="text" placeholder="User Name" name="username" required />
            <input type="email" placeholder="Email" name="email" required />
            <input type="text" placeholder="Phone Number" name="phone" required />
            <input type="password" placeholder="Passcode" name="passcode" autocomplete="new-password" required />
            <input type="password" placeholder="Confirm Passcode" name="confirmpasscode" autocomplete="new-password" required />


            <input type="submit" value="Register" name="register" class="btn btn-block btn-primary" />
            <br>
            <a href="login.php" class="btn btn-block btn-primary">Login</a>
        </form>
    </div>
</div>